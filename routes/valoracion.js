var async = require('async');

//Añadir una valoracion
exports.nuevaValoracion = function (req, res) {

    var valoracion = connection.escape(req.body.valoracion);
    var nota = connection.escape(req.body.nota);
    var idDelSitio = connection.escape(req.body.idSitio);
    var titulo = connection.escape(req.body.titulo);
    var idUsuario = connection.escape(req.body.id);

    console.log('insert into valoraciones (valoracion,nota,idDelSitio,usuarios_idUsuario, titulo) values (' + valoracion + ',' + nota + ','+idDelSitio+','+idUsuario+','+titulo+');');
    connection.query('insert into valoraciones (valoracion,nota,idDelSitio,usuarios_idUsuario, titulo) values (' + valoracion + ',' + nota + ','+idDelSitio+','+idUsuario+','+titulo+');', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("Valoracion creada");
            res.send("Valoracion creada");
        }
    });
}

//Lista de valroaciones
exports.listaValoraciones = function (req, res) {

    var idDelSitio = connection.escape(req.body.idSitio);

    console.log('select titulo, idValoracion from valoraciones where idDelSitio = ' + idDelSitio);
    connection.query('select titulo, idValoracion from valoraciones where idDelSitio = ' + idDelSitio, function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            var valoracionesA = new Array();
            for (var x in results) {
                valoracionesA[x] = { titulo: results[x].titulo, id: results[x].idValoracion };
            }
            console.log(valoracionesA);
            res.json(valoracionesA);
        }
    });
}

//Valoracion

exports.valoracion = function (req, res) {

    var id = connection.escape(req.body.id);

    console.log('select titulo, valoracion, nota from valoraciones where idValoracion = ' + id);
    connection.query('select titulo, valoracion, nota from valoraciones where idValoracion = ' + id, function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            var valoracion = [{ titulo: results[0].titulo, valoracion: results[0].valoracion, nota: results[0].nota }];
            console.log(valoracion);
            res.json(valoracion);
        }
    });
}