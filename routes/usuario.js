var async = require('async');
var crypto = require('crypto');
var salt = 'Sv+|~F53hvuNiYB*.f,8WQJOj~j/%bmtXFx,#M|.3[PN@jK?Sj5?0XXX!h=f^9%|';
//Dar de alta un nuevo usuario
exports.alta = function (req, res) {

    //Se recogen los datos enviados y se escapan para evitar SQLInjection
    var email = connection.escape(req.body.email);
    var password = connection.escape(req.body.pass);
    var usuario = connection.escape(req.body.usuario);

    
    //connection.connect();
    //Se inserta en la base de datos
    console.log(email);
    console.log(usuario);
    var sha1 = crypto.createHash('sha1');
    async.series([
            function (callback) {
                comprobarEmailRepetido(email, callback);
            },
            function (callback) {
                comprobarNombreDeUsuario(email, callback);
            }
        ],
        function (err, results) {
            console.log(results[0] + " " + results[1]);
            if (!results[0]) {
                res.send("El email ya esta dado de alta");
            } else if (!results[1]) {
                res.send("El nombre de usuario ya existe");
            } else {
                //Encriptar contraseña con sha1
                var passSalt = password + salt;
                var sha1 = crypto.createHash('sha1');
                var prueba = sha1.update(passSalt);
                var passEncriptada = prueba.digest('hex');
                insertar(email, usuario, passEncriptada, res);
            }

        });

    //connection.end();
};

//Funcion para comprobar si el email está repetido
function comprobarEmailRepetido(email, callback) {
    //Se hace la consulta a la base de datos para comprobar si el email ya existe
    connection.query('select * from usuarios where email like ' + email + ';', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("results1 es: " + results.length);
            //Si results es mayor qeu 0 quiere decir que el email ya existe y se devuelve false
            if (results.length > 0) {
                return callback(null, false);
            } else {
                //Como results es igual a 0 quiere decir que el email no exite y se devuelve true
                return callback(null, true);
            }
        }
    });
}

//Funcion para comprobar si el nombre de usuario ya existe
function comprobarNombreDeUsuario(usuario, callback) {
    //Se hace la consulta a la base de datos para comprobar si el nombre de usuario ya existe
    connection.query('select * from usuarios where nombreDeUsuario like ' + usuario + ';', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("results2 es: " + results.length);
            //Si results es mayor qeu 0 quiere decir que el nombre ya existe y se devuelve false
            if (results.length > 0) {
                return callback(null, false);
            } else {
                //Como results es igual a 0 quiere decir que el nombre no exite y se devuelve true
                return callback(null, true);
            }
        }
    });
}

function insertar(email, usuario, password,res) {
    console.log('insert into usuarios (email,nombreDeUsuario,password) values (' + email + ',' + usuario + ',"' + password + '");');
    connection.query('insert into usuarios (email,nombreDeUsuario,password) values (' + email + ',' + usuario + ',"' + password + '");', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("Usuario nuevo");
            res.send("Usuario creado");
        }
    });
}

exports.login = function (req, res) {
    var email = connection.escape(req.body.email);
    var password = connection.escape(req.body.pass);
    var passSalt = password + salt;
    var sha1 = crypto.createHash('sha1');
    var prueba = sha1.update(passSalt);
    var passEncriptada = prueba.digest('hex');
    console.log('select * from usuarios where email like ' + email + ' and password like "' + passEncriptada + '";');
    connection.query('select * from usuarios where email like ' + email + ' and password like "' + passEncriptada + '";', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("results es: " + results[0]);
            //Si results es mayor qeu 0 quiere decir que el nombre ya existe y se devuelve false
            if (results.length == 0) {
                var respuesta = [{ id: "null", nombre: "Email o contraseña incorrectos"}];
                res.json(respuesta);
            } else {
                //Como results es igual a 0 quiere decir que el nombre no exite y se devuelve true
                var usuario = [{ id: results[0].idUsuario, nombre: results[0].nombreDeUsuario}];
                res.json(usuario);
            }
        }
    });
}