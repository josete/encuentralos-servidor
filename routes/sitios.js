exports.nuevoSitio = function (req, res) {

    var nombre = connection.escape(req.body.nombre);
    var tipo = connection.escape(req.body.tipo);
    var direccion = connection.escape(req.body.direccion);
    var lat = connection.escape(Math.round(req.body.lat));
    var lon = connection.escape(Math.round(req.body.lon));
    var idUsuario = connection.escape(req.body.idUsuario);

    console.log('insert into sitios (tipoDeSitio,nombreDeSitio,latitud,longitud,direccion,usuarios_idUsuario) values (' + tipo + ',' + nombre + ',' + lat + ',' + lon + ',' + direccion + ',' + idUsuario + ');');
    
    connection.query('insert into sitios (tipoDeSitio,nombreDeSitio,latitud,longitud,direccion,usuarios_idUsuario) values (' + tipo + ',' + nombre + ',' + lat + ',' + lon + ',' + direccion + ',' + idUsuario + ');', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("Sitio guardado");
            res.send("Sitio guardado");
        }
    });

}

exports.getSitios = function (req, res) {

    var lat = connection.escape(Math.round(req.body.lat));
    var lon = connection.escape(Math.round(req.body.lon));
    var tipo = connection.escape(req.body.tipo);

    console.log('select * from sitios where latitud = ' + lat + ' and longitud = ' + lon + ' and tipoDeSitio = ' + tipo);

    connection.query('select * from sitios where latitud = ' + lat + ' and longitud = ' + lon + ' and tipoDeSitio = ' + tipo, function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            var sitios = new Array();
            for (var x in results) {
                sitios[x] = { id: results[x].idSitio, nombre: results[x].nombreDeSitio, direccion: results[x].direccion };
            }
            console.log(sitios);
            res.json(sitios);
        }
    });

}

exports.getSitio = function (req, res) {
    var id = connection.escape(req.body.id);
    console.log('select * from sitios where idSitio = ' + id);
    connection.query('select * from sitios where idSitio =' + id, function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            var sitio = [{nombre: results[0].nombreDeSitio, direccion: results[0].direccion, lat: results[0].latitud, lon: results[0].longitud}];
            console.log(sitio);
            res.json(sitio);
        }
    });
}