var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var Buffer = require('buffer').Buffer;

exports.subirFoto = function (req, res) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];
    form.uploadDir = './public/images';
    form.keepExtensions = true;
    form
      .on('file', function (field, file) {
          files.push([field, file]);
      })
      .on('end', function () {
          console.log('Upload terminado');
          var path = util.inspect(files[0][1].path);
          var partes = path.split("\\");
          var nombreExt = partes[partes.length - 1];
          var partes2 = nombreExt.split(".");
          var nombre = partes2[0];
          res.writeHead(200, { 'content-type': 'text/plain' });
          res.end(nombre);
      });
    form.parse(req);

}

exports.descargarFotos = function(req,res){
    
    fs.open("file.txt", 'r', function(status, fd) {
    if (status) {
        console.log(status.message);
        return;
    }
    var buffer = new Buffer(100);
    //buffer son los bytes de la foto
    fs.read(fd, buffer, 0, 100, 0, function(err, num) {
        console.log(buffer.toString('utf-8', 0, num));
    });
});
}

exports.insertarFoto =  function (req,res){
    var idSitio = connection.escape(req.body.idSitio);
    var idUsuario = connection.escape(req.body.idUsuario);
    var ruta = connection.escape(req.body.ruta);

    console.log('insert into fotografias (idDelSitio,imagen,Usuarios_idUsuario) values (' + idSitio + ',' + ruta + ',' + idUsuario + ');');
    connection.query('insert into fotografias (idDelSitio,imagen,Usuarios_idUsuario) values (' + idSitio + ',' + ruta + ',' + idUsuario + ');', function (err, results, fields) {
        if (err) {
            console.log("Error: " + err.message);
            throw err;
        } else {
            console.log("Fotografia guardada");
            res.send("Fotografia guardada");
        }
    });
  }  