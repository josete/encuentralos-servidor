
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var usuario = require('./routes/usuario');
var valoracion = require('./routes/valoracion');
var imagen = require('./routes/fotos');
var sitio = require('./routes/sitios');
var http = require('http');
var path = require('path');
var mysql = require('mysql');
var app = express();
//Conexion a la base de datos
connection = mysql.createConnection({
                    host : 'localhost',
                    user : 'root',
                    password : 'root',
                    database: 'encuentralos'
});

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

/*********************POST*******************/
//Llamada a la funcion para dar de alta un usuario
app.post('/insertarUsuario', usuario.alta);
//Llamada a la funcion del login
app.post('/login',usuario.login);
//Llamada a la funcion para crear una valoracion
app.post('/nuevaValoraion',valoracion.nuevaValoracion);

app.post('/subirFoto', imagen.subirFoto);
app.post('/insertarFoto', imagen.insertarFoto);

app.post('/nuevoSitio', sitio.nuevoSitio);
app.post('/getSitios', sitio.getSitios);
app.post('/getSitio', sitio.getSitio);

/*********************GET*********************/
app.post('/getValoracion',valoracion.listaValoraciones);
app.post('/verValoracion',valoracion.valoracion);


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
